package com.chikeandroid.tutsplus_glide

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView

import com.bumptech.glide.Glide

/**
 * Created by Chike on 2/12/2017.
 */

class GifsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gifs)

        val gifImageView = findViewById(R.id.iv_gif) as ImageView

        Glide.with(this)
                .load("http://i.imgur.com/Vth6CBz.gif")
                .asGif()
                .placeholder(R.drawable.ic_cloud_off_red)
                .error(R.drawable.ic_cloud_off_red)
                .into(gifImageView)
    }

}
