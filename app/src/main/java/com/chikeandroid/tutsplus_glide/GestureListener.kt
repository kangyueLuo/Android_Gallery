package com.chikeandroid.tutsplus_glide

import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent

import com.chikeandroid.tutsplus_glide.R.anim.slide_in_left
import com.chikeandroid.tutsplus_glide.R.anim.slide_in_right
import com.chikeandroid.tutsplus_glide.R.anim.slide_out_left
import com.chikeandroid.tutsplus_glide.R.anim.slide_out_right


/**
 * Created by luokangyu on 25/07/2018.
 */

class GestureListener : GestureDetector.SimpleOnGestureListener() {
    private val SWIPE_MIN_DISTANCE = 120
    private val SWIPE_THRESHOLD_VELOCITY = 200

    override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {

        val m = (e1.y - e2.y) / (e1.x - e2.x)
        Log.d(TAG, "m : $m") // 透過斜率判斷 滑度方向

        if (m < 0.0f) {

            if (e1.x - e2.x > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                // Right to left, your code here

                Log.d(TAG, "Show Next 1 ,right in left out")
                //    photoSwitcher.setInAnimation(slide_in_right);
                //    photoSwitcher.setOutAnimation(slide_out_left);
                //    imageHandler.sendEmptyMessage(SHOW_NEXT);

            }

            if (e2.x - e1.x > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

                Log.d(TAG, "Show Pre 2left in right out")

                //    photoSwitcher.setInAnimation(slide_in_left);
                //    photoSwitcher.setOutAnimation(slide_out_right);
                //    imageHandler.sendEmptyMessage(SHOW_PRE);

            }
        }

        if (m > 0.0f) {
            if (e2.x - e1.x > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                // Left to right, your code here

                Log.d(TAG, "Show Pre 3,left in right out")
                //    photoSwitcher.setInAnimation(slide_in_left);
                //    photoSwitcher.setOutAnimation(slide_out_right);
                //    imageHandler.sendEmptyMessage(SHOW_PRE);

            }

            if (e1.x - e2.x > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                // Left to right, your code here

                Log.d(TAG, "Show Next 4 , right in left out")
                //    photoSwitcher.setInAnimation(slide_in_right);
                //    photoSwitcher.setOutAnimation(slide_out_left);
                //    imageHandler.sendEmptyMessage(SHOW_NEXT);

            }
        }
        return false
    }

    companion object {

        private val TAG = "GestureListener"
    }
}
