package com.chikeandroid.tutsplus_glide

import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.graphics.Palette
import android.view.ViewGroup
import android.widget.ImageView

import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target

/**
 * Created by Chike on 2/12/2017.
 */

class PhotoActivity : AppCompatActivity() {

    private var mImageView: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_detail)

        mImageView = findViewById(R.id.image) as ImageView
        val spacePhoto = intent.getParcelableExtra<Resource>(EXTRA_SPACE_PHOTO)

        Glide.with(this)
                .load(spacePhoto.url)
                .asBitmap()
                .error(R.drawable.ic_cloud_off_red)
                .listener(object : RequestListener<String, Bitmap> {

                    override fun onException(e: Exception, model: String, target: Target<Bitmap>, isFirstResource: Boolean): Boolean {
                        return false
                    }

                    override fun onResourceReady(resource: Bitmap, model: String, target: Target<Bitmap>, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {

                        onPalette(Palette.from(resource).generate())
                        mImageView!!.setImageBitmap(resource)

                        return false
                    }

                    fun onPalette(palette: Palette?) {
                        if (null != palette) {
                            val parent = mImageView!!.parent.parent as ViewGroup
                            parent.setBackgroundColor(palette.getDarkVibrantColor(Color.GRAY))
                        }
                    }
                })
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(mImageView!!)

    }

    companion object {

        val EXTRA_SPACE_PHOTO = "PhotoActivity.SPACE_PHOTO"
    }

    /* private SimpleTarget target = new SimpleTarget<Bitmap>() {

        @Override
        public void onResourceReady(Bitmap bitmap, GlideAnimation glideAnimation) {

           onPalette(Palette.from(bitmap).generate());
           mImageView.setImageBitmap(bitmap);
        }

        public void onPalette(Palette palette) {
            if (null != palette) {
                ViewGroup parent = (ViewGroup) mImageView.getParent().getParent();
                parent.setBackgroundColor(palette.getDarkVibrantColor(Color.GRAY));
            }
        }
    };*/
}
