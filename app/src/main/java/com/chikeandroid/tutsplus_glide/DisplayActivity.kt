package com.chikeandroid.tutsplus_glide

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.graphics.Palette
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.VideoView
import android.widget.ViewSwitcher

import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target

import java.io.File

class DisplayActivity : AppCompatActivity() {
    internal lateinit var viewSwitcher: ViewSwitcher
    internal var filePath: String? = null
    internal var fileID = 0
    internal var imageView: ImageView? = null
    internal var gifImageView: ImageView? = null
    internal var videoView: VideoView? = null
    internal var temp: File? = null
    internal var slide_in_left: Animation? = null
    internal var slide_out_right: Animation? = null
    internal var slide_in_right: Animation? = null
    internal var slide_out_left: Animation? = null
    internal var photoDrawable: Drawable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display)

        viewSwitcher = findViewById(R.id.switcher) as ViewSwitcher
        slide_in_left = AnimationUtils.loadAnimation(this, R.anim.slide_in_left)
        slide_out_right = AnimationUtils.loadAnimation(this, R.anim.slide_out_right)
        slide_in_right = AnimationUtils.loadAnimation(this, R.anim.slide_in_right)
        slide_out_left = AnimationUtils.loadAnimation(this, R.anim.slide_out_left)

        val bundle = this.intent.extras
        filePath = bundle!!.getString("FILE_PATH")
        fileID = bundle.getInt("FILE_ID")
        Log.d(TAG, "filePath : " + filePath!!)
        Log.d(TAG, "fileID : $fileID")

        temp = File(filePath!!)
        when (getFileTail(temp!!)) {
            "jpg" -> {
                addImageView()
            }

            "png" -> {
                addImageView()
            }
            "mp4" -> {
                addVideoView()
            }
            "gif" -> {
                addGifView()
            }
        }


    }

    private fun getFileTail(file: File): String? {
        var fileTail: String? = null
        if (file.name.lastIndexOf(".") > 0) {
            fileTail = file.name.substring(file.name.lastIndexOf(".") + 1)
        }
        Log.d(TAG, "fileTail : " + fileTail!!)
        return fileTail
    }

    private fun addImageView() {

        viewSwitcher.removeAllViews()
        imageView = ImageView(this)
        imageView!!.scaleType = ImageView.ScaleType.FIT_CENTER

        Glide.with(this)
                .load(filePath)
                .asBitmap()
                .error(R.drawable.ic_cloud_off_red)
                .listener(object : RequestListener<String, Bitmap> {

                    override fun onException(e: Exception, model: String, target: Target<Bitmap>, isFirstResource: Boolean): Boolean {
                        return false
                    }

                    override fun onResourceReady(resource: Bitmap, model: String, target: Target<Bitmap>, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {

                        onPalette(Palette.from(resource).generate())
                        imageView!!.setImageBitmap(resource)

                        return false
                    }

                    fun onPalette(palette: Palette?) {
                        if (null != palette) {
                            val parent = imageView!!.parent.parent as ViewGroup
                            parent.setBackgroundColor(palette.getDarkVibrantColor(Color.GRAY))
                        }
                    }
                })
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(imageView!!)

        viewSwitcher.addView(imageView)
    }

    private fun addVideoView() {

        viewSwitcher.removeAllViews()
        val lp = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        val lp2 = FrameLayout.LayoutParams(lp)
        lp2.gravity = Gravity.CENTER
        videoView = VideoView(this)
        videoView!!.layoutParams = lp2
        videoView!!.setVideoPath(filePath)
        viewSwitcher.addView(videoView)
        videoView!!.start()
    }

    private fun addGifView() {

        viewSwitcher.removeAllViews()

        gifImageView = ImageView(this)
        gifImageView!!.scaleType = ImageView.ScaleType.FIT_CENTER
        Glide.with(this)
                .load(filePath)
                .asGif()
                .placeholder(R.drawable.ic_cloud_off_red)
                .error(R.drawable.ic_cloud_off_red)
                .into(gifImageView!!)
        viewSwitcher.addView(gifImageView)
    }

    companion object {

        private val TAG = "DisplayActivity"
    }
}
