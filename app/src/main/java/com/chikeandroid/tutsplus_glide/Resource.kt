package com.chikeandroid.tutsplus_glide

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Chike on 2/11/2017.
 */

class Resource : Parcelable {

    var url: String? = null
    var title: String? = null

    constructor(url: String, title: String) {
        this.url = url
        this.title = title
    }

    protected constructor(`in`: Parcel) {
        url = `in`.readString()
        title = `in`.readString()
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, i: Int) {
        parcel.writeString(url)
        parcel.writeString(title)
    }

    companion object {

        val CREATOR: Parcelable.Creator<Resource> = object : Parcelable.Creator<Resource> {
            override fun createFromParcel(`in`: Parcel): Resource {
                return Resource(`in`)
            }

            override fun newArray(size: Int): Array<Resource?> {
                return arrayOfNulls(size)
            }
        }

        // new Resource("/storage/emulated/0/Download/Linda_MIX_2S.mp4","Linda_MIX_2S"), // 在下載資料夾放 檔案
        val resources: Array<Resource>
            get() = arrayOf(Resource("http://i.imgur.com/zuG2bGQ.jpg", "Galaxy"), Resource("http://i.imgur.com/ovr0NAF.jpg", "Space Shuttle"), Resource("http://i.imgur.com/n6RfJX2.jpg", "Galaxy Orion"), Resource("http://i.imgur.com/qpr5LR2.jpg", "Earth"), Resource("http://i.imgur.com/pSHXfu5.jpg", "Astronaut"), Resource("http://i.imgur.com/3wQcZeY.jpg", "Satellite"), Resource("http://i.imgur.com/Vth6CBz.gif", "GifTest"), Resource("http://i.imgur.com/WVClSw8.png", "paper"))
    }

     object CREATOR : Parcelable.Creator<Resource> {
        override fun createFromParcel(parcel: Parcel): Resource {
            return Resource(parcel)
        }

        override fun newArray(size: Int): Array<Resource?> {
            return arrayOfNulls(size)
        }
    }

}
