package com.chikeandroid.tutsplus_glide

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

import com.bumptech.glide.Glide
import com.chikeandroid.tutsplus_glide.GalleryActivity.ImageGalleryAdapter

import java.io.File

/**
 * Created by Chike on 2/12/2017.
 */

class GalleryActivity : AppCompatActivity() {

    private val TAG = "GalleryActivity"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_space_gallery)

        val imageGalleryAdapter : ImageGalleryAdapter = ImageGalleryAdapter(this,Resource.resources);

        requestStoragePermission()

        val layoutManager = GridLayoutManager(this, 2)
        val recyclerView = findViewById(R.id.rv_images) as RecyclerView
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = layoutManager

        val adapter = imageGalleryAdapter
        recyclerView.adapter = adapter

    }

     inner class ImageGalleryAdapter(private val mContext: Context, private val mResources: Array<Resource>) : RecyclerView.Adapter<ImageGalleryAdapter.MyViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageGalleryAdapter.MyViewHolder {

            val context = parent.context
            val inflater = LayoutInflater.from(context)

            // Inflate the layout
            val photoView = inflater.inflate(R.layout.item_photo, parent, false)

            return MyViewHolder(photoView)
        }

        override fun onBindViewHolder(holder: ImageGalleryAdapter.MyViewHolder, position: Int) {

            val resource = mResources[position]
            val imageView = holder.mPhotoImageView

            Glide.with(mContext)
                    .load(resource.url)
                    .placeholder(R.drawable.ic_cloud_off_red)
                    .into(imageView)
        }

        override fun getItemCount(): Int {
            items = mResources.size
            return items
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

            var mPhotoImageView: ImageView

            init {
                mPhotoImageView = itemView.findViewById(R.id.iv_photo) as ImageView
                itemView.setOnClickListener(this)
            }

            override fun onClick(view: View) {

                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val resource = mResources[position]
                    Log.d(TAG, resource.url)

                    val filePath = resource.url
                    val tempFile = File(filePath)

                    // 判斷 檔案類型
                    //                    switch (getFileTail(tempFile))
                    //                    {
                    //                        case "gif":{
                    //                            Intent intent = new Intent(mContext, GifsActivity.class);
                    //                            intent.putExtra(PhotoActivity.EXTRA_SPACE_PHOTO, resource);
                    //                            startActivity(intent);
                    //                        }
                    //                        break;
                    //
                    //                        case "mp4":{
                    //                            Intent intent = new Intent(mContext,VideoActivity.class);
                    //                            intent.putExtra("videoPath",filePath);
                    //                            startActivity(intent);
                    //                        }
                    //                        break;
                    //
                    //                        case "png":{
                    //                            Intent intent = new Intent(mContext, PhotoActivity.class);
                    //                            intent.putExtra(PhotoActivity.EXTRA_SPACE_PHOTO, resource);
                    //                            startActivity(intent);
                    //                        }
                    //
                    //                        case "jpg":{
                    //                            Intent intent = new Intent(mContext, PhotoActivity.class);
                    //                            intent.putExtra(PhotoActivity.EXTRA_SPACE_PHOTO, resource);
                    //                            startActivity(intent);
                    //                        }
                    //                    }

                    val it2Display = Intent(mContext, DisplayActivity::class.java)
                    val bundle = Bundle()
                    bundle.putString("FILE_PATH", filePath)
                    bundle.putInt("FILE_ID", position)
                    it2Display.putExtras(bundle)
                    startActivity(it2Display)

                }
            }
        }


        private fun getmResources(): Array<Resource> {
            return mResources
        }


    }

    private fun requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this@GalleryActivity,
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this@GalleryActivity,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    MY_PERMISSIONS_REQUEST)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // file-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return
            }
        }

    }

    companion object {
        private val MY_PERMISSIONS_REQUEST = 100
        var items = 0
    }
}
